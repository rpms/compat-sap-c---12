%global __python /usr/bin/python3
%global scl 1
%global scl_prefix compat-sap-
%global _root_prefix /opt/rh/SAP
%global _root_infodir %{_root_prefix}/%{_infodir}
%global _root_mandir %{_root_prefix}/%{_mandir}
%{?scl:%global __strip strip}
%{?scl:%global __objdump objdump}
%global DATE 20220507
%global gitrev fa107326a13af9a7d7aa0df28fe364db0f6fb171
%global gcc_version 12.1.1
%global gcc_major 12
# Note, gcc_release must be integer, if you want to add suffixes to
# %%{release}, append them after %%{gcc_release} on Release: line.
%global gcc_release 1
%global _unpackaged_files_terminate_build 0
%undefine _annotated_build
%global multilib_64_archs sparc64 ppc64 x86_64
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le ppc64p7 s390 s390x %{arm} aarch64
%global attr_ifunc 1
%else
%global attr_ifunc 0
%endif
%ifarch x86_64
%global multilib_32_arch i686
%endif
%if 0%{?fedora} >= 36 || 0%{?rhel} >= 10
%global build_annobin_plugin 1
%else
%global build_annobin_plugin 0
%endif
Summary: C++ compatibility runtime library for SAP applications
Name: %{?scl_prefix}c++-12
ExclusiveArch: x86_64 ppc64le s390x
Version: %{gcc_version}
Release: %{gcc_release}.2%{?dist}
# libgcc, libgfortran, libgomp, libstdc++ and crtstuff have
# GCC Runtime Exception.
License: GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD
# The source for this package was pulled from upstream's vcs.
# %%{gitrev} is some commit from the
# https://gcc.gnu.org/git/?p=gcc.git;h=refs/vendors/redhat/heads/gcc-%%{gcc_major}-branch
# branch.  Use the following commands to generate the tarball:
# git clone --depth 1 git://gcc.gnu.org/git/gcc.git gcc-dir.tmp
# git --git-dir=gcc-dir.tmp/.git fetch --depth 1 origin %%{gitrev}
# git --git-dir=gcc-dir.tmp/.git archive --prefix=%%{name}-%%{version}-%%{DATE}/ %%{gitrev} | xz -9e > %%{name}-%%{version}-%%{DATE}.tar.xz
# rm -rf gcc-dir.tmp
Source0: gcc-%{version}-%{DATE}.tar.xz
URL: http://gcc.gnu.org
# Need binutils with -pie support >= 2.14.90.0.4-4
# Need binutils which can omit dot symbols and overlap .opd on ppc64 >= 2.15.91.0.2-4
# Need binutils which handle -msecure-plt on ppc >= 2.16.91.0.2-2
# Need binutils which support .weakref >= 2.16.91.0.3-1
# Need binutils which support --hash-style=gnu >= 2.17.50.0.2-7
# Need binutils which support mffgpr and mftgpr >= 2.17.50.0.2-8
# Need binutils which support --build-id >= 2.17.50.0.17-3
# Need binutils which support %%gnu_unique_object >= 2.19.51.0.14
# Need binutils which support .cfi_sections >= 2.19.51.0.14-33
# Need binutils which support --no-add-needed >= 2.20.51.0.2-12
# Need binutils which support -plugin
# Need binutils which support .loc view >= 2.30
# Need binutils which support --generate-missing-build-notes=yes >= 2.31
BuildRequires: binutils >= 2.19.51.0.14-33
# While gcc doesn't include statically linked binaries, during testing
# -static is used several times.
BuildRequires: glibc-static
BuildRequires: zlib-devel, gettext, dejagnu, bison, flex, sharutils
BuildRequires: texinfo, texinfo-tex, /usr/bin/pod2man
BuildRequires: gcc, gcc-c++, make
# For VTA guality testing
BuildRequires: gdb
# Make sure pthread.h doesn't contain __thread tokens
# Make sure glibc supports stack protector
# Make sure glibc supports DT_GNU_HASH
BuildRequires: glibc-devel >= 2.4.90-13
BuildRequires: elfutils-devel >= 0.147
BuildRequires: elfutils-libelf-devel >= 0.147
%if 0%{?rhel} >= 8
BuildRequires: libzstd-devel
%endif
%ifarch ppc ppc64 ppc64le ppc64p7 s390 s390x sparc sparcv9 alpha
# Make sure glibc supports TFmode long double
BuildRequires: glibc >= 2.3.90-35
%endif
%ifarch %{multilib_64_archs} sparcv9 ppc
# Ensure glibc{,-devel} is installed for both multilib arches
BuildRequires: /lib/libc.so.6 /usr/lib/libc.so /lib64/libc.so.6 /usr/lib64/libc.so
%endif
%ifarch ia64
BuildRequires: libunwind >= 0.98
%endif
# Need .eh_frame ld optimizations
# Need proper visibility support
# Need -pie support
# Need --as-needed/--no-as-needed support
# On ppc64, need omit dot symbols support and --non-overlapping-opd
# Need binutils that owns /usr/bin/c++filt
# Need binutils that support .weakref
# Need binutils that supports --hash-style=gnu
# Need binutils that support mffgpr/mftgpr
# Need binutils that support --build-id
# Need binutils that support %%gnu_unique_object
# Need binutils that support .cfi_sections
# Need binutils that support --no-add-needed
# Need binutils that support -plugin
# Need binutils that support .loc view >= 2.30
# Need binutils which support --generate-missing-build-notes=yes >= 2.31
# Don't require this for compat-sap-c++ (#1491573).
#Requires: binutils >= 2.19.51.0.14-33
# Make sure gdb will understand DW_FORM_strp
Conflicts: gdb < 5.1-2
# Don't require this for compat-sap-c++ (#1491573).
#Requires: glibc-devel >= 2.2.90-12
%ifarch ppc ppc64 ppc64le ppc64p7 s390 s390x sparc sparcv9 alpha
# Make sure glibc supports TFmode long double
Requires: glibc >= 2.3.90-35
%endif
Requires: libgcc >= 4.1.2-43
# Keep this for now.
Requires: libgomp >= 4.4.4-13
BuildRequires: gmp-devel >= 4.1.2-8
BuildRequires: mpfr-devel >= 3.1.0
%if 0%{?rhel} >= 7
BuildRequires: libmpc-devel >= 0.8.1
%endif

%if 0%{?rhel} == 8
%ifarch ppc64le
BuildRequires: gcc-toolset-12-runtime gcc-toolset-12-binutils
BuildRequires: gcc-toolset-12-gcc gcc-toolset-12-gcc-c++
%endif
%endif

AutoReq: true
AutoProv: false
# Don't provide this for compat-sap-c++ (#1491573).
#%ifarch sparc64 ppc64 ppc64le s390x x86_64 ia64
#Provides: liblto_plugin.so.0()(64bit)
#%else
#Provides: liblto_plugin.so.0
#%endif
%global oformat %{nil}
%global oformat2 %{nil}
%ifarch %{ix86}
%global oformat OUTPUT_FORMAT(elf32-i386)
%endif
%ifarch x86_64
%global oformat OUTPUT_FORMAT(elf64-x86-64)
%global oformat2 OUTPUT_FORMAT(elf32-i386)
%endif
%ifarch ppc
%global oformat OUTPUT_FORMAT(elf32-powerpc)
%global oformat2 OUTPUT_FORMAT(elf64-powerpc)
%endif
%ifarch ppc64
%global oformat OUTPUT_FORMAT(elf64-powerpc)
%global oformat2 OUTPUT_FORMAT(elf32-powerpc)
%endif
%ifarch s390
%global oformat OUTPUT_FORMAT(elf32-s390)
%endif
%ifarch s390x
%global oformat OUTPUT_FORMAT(elf64-s390)
%global oformat2 OUTPUT_FORMAT(elf32-s390)
%endif
%ifarch ia64
%global oformat OUTPUT_FORMAT(elf64-ia64-little)
%endif
%ifarch ppc64le
%global oformat OUTPUT_FORMAT(elf64-powerpcle)
%endif
%ifarch aarch64
%global oformat OUTPUT_FORMAT(elf64-littleaarch64)
%endif

Patch0: gcc12-hack.patch
Patch2: gcc12-sparc-config-detection.patch
Patch3: gcc12-libgomp-omp_h-multilib.patch
Patch4: gcc12-libtool-no-rpath.patch
Patch5: gcc12-isl-dl.patch
Patch6: gcc12-isl-dl2.patch
Patch7: gcc12-libstdc++-docs.patch
Patch8: gcc12-no-add-needed.patch
Patch9: gcc12-Wno-format-security.patch
Patch10: gcc12-rh1574936.patch
Patch11: gcc12-d-shared-libphobos.patch

Patch3000: 0001-basic_string-reserve-n-semantics-are-not-available-i.patch
Patch3001: 0004-operator-istream-char-N-eofbit-fixes-are-not-availab.patch
Patch3002: 0005-Disable-tests-for-PR-libstdc-79820-and-PR-libstdc-81.patch
Patch3003: 0006-Don-t-assume-has_facet-codecvt_c16-when-run-against-.patch
Patch3004: 0008-testsuite-build-plugins-with-std-c-11.patch
Patch3005: 0009-Fix-22_locale-locale-cons-unicode.cc-when-run-under-.patch
Patch3006: 0010-Don-t-verify-exception-handling-in-basic_filebuf-clo.patch
Patch3007: 0011-Add-dts.exp-and-use-it-to-fix-22_locale-messages-136.patch
Patch3008: 0012-dts.exp-use-usr-bin-gcc.patch
Patch3009: 0013-Rename-__CXXSTDLIB_SO_VERSION__-to-__LIBSTDCXX_SO_VE.patch
Patch3010: 0014-Conditionalize-tests-for-PR-libstdc-98466-on-__LIBST.patch
Patch3011: 0015-Conditionalize-test-for-PR-libstdc-87135-on-__LIBSTD.patch
Patch3012: 0016-Conditionalize-test-for-hashtable-bucket-sizes-on-__.patch
Patch3013: 0017-Conditionalize-test-for-PR-libstdc-71181-on-__LIBSTD.patch
Patch3014: gcc12-dg-ice-fixes.patch

%if 0%{?rhel} == 9
%global nonsharedver 11
%endif
%if 0%{?rhel} == 8
%global nonsharedver 80
%endif
%if 0%{?rhel} == 7
%global nonsharedver 48
%endif
%if 0%{?rhel} == 6
%global nonsharedver 44
%endif

%if 0%{?scl:1}
%global _gnu %{nil}
%else
%global _gnu -gnueabi
%endif
%ifarch sparcv9
%global gcc_target_platform sparc64-%{_vendor}-%{_target_os}
%endif
%ifarch ppc ppc64p7
%global gcc_target_platform ppc64-%{_vendor}-%{_target_os}
%endif
%ifnarch sparcv9 ppc ppc64p7
%global gcc_target_platform %{_target_platform}
%endif

%description
This package provides runtime compatibility libraries for use by SAP
application binaries only.

%prep
%setup -q -n gcc-%{version}-%{DATE}
%patch0 -p0 -b .hack~
%patch2 -p0 -b .sparc-config-detection~
%patch3 -p0 -b .libgomp-omp_h-multilib~
%patch4 -p0 -b .libtool-no-rpath~
%patch8 -p0 -b .no-add-needed~
%patch9 -p0 -b .Wno-format-security~
%if 0%{?fedora} >= 29 || 0%{?rhel} > 7
%patch10 -p0 -b .rh1574936~
%endif
%patch11 -p0 -b .d-shared-libphobos~

# Apply DTS-specific testsuite patches.
%patch3000 -p1 -b .dts-test-0~
%patch3001 -p1 -b .dts-test-1~
%patch3002 -p1 -b .dts-test-2~
%patch3003 -p1 -b .dts-test-3~
%patch3004 -p1 -b .dts-test-4~
%patch3005 -p1 -b .dts-test-5~
%patch3006 -p1 -b .dts-test-6~
%patch3007 -p1 -b .dts-test-7~
%patch3008 -p1 -b .dts-test-8~
%patch3009 -p1 -b .dts-test-9~
%patch3010 -p1 -b .dts-test-10~
%patch3011 -p1 -b .dts-test-11~
%patch3012 -p1 -b .dts-test-12~
%patch3013 -p1 -b .dts-test-13~
%patch3014 -p1 -b .dts-test-14~

find gcc/testsuite -name \*.pr96939~ | xargs rm -f

echo 'Red Hat %{version}-%{gcc_release}' > gcc/DEV-PHASE

%if 0%{?rhel} == 6
# Default to -gdwarf-3 rather than -gdwarf-5
sed -i '/UInteger Var(dwarf_version)/s/Init(5)/Init(3)/' gcc/common.opt
sed -i 's/\(version for most targets is \)5 /\13 /' gcc/doc/invoke.texi
%endif
%if 0%{?rhel} <= 8
# Default to -gdwarf-4 rather than -gdwarf-5
sed -i '/UInteger Var(dwarf_version)/s/Init(5)/Init(4)/' gcc/common.opt
sed -i 's/\(version for most targets is \)5 /\14 /' gcc/doc/invoke.texi
%endif

cp -a libstdc++-v3/config/cpu/i{4,3}86/atomicity.h
cp -a libstdc++-v3/config/cpu/i{4,3}86/opt
echo 'TM_H += $(srcdir)/config/rs6000/rs6000-modes.h' >> gcc/config/rs6000/t-rs6000

./contrib/gcc_update --touch

LC_ALL=C sed -i -e 's/\xa0/ /' gcc/doc/options.texi

sed -i -e 's/Common Driver Var(flag_report_bug)/& Init(1)/' gcc/common.opt
sed -i -e 's/context->report_bug = false;/context->report_bug = true;/' gcc/diagnostic.cc

%ifarch ppc
if [ -d libstdc++-v3/config/abi/post/powerpc64-linux-gnu ]; then
  mkdir -p libstdc++-v3/config/abi/post/powerpc64-linux-gnu/64
  mv libstdc++-v3/config/abi/post/powerpc64-linux-gnu/{,64/}baseline_symbols.txt
  mv libstdc++-v3/config/abi/post/powerpc64-linux-gnu/{32/,}baseline_symbols.txt
  rm -rf libstdc++-v3/config/abi/post/powerpc64-linux-gnu/32
fi
%endif
%ifarch sparc
if [ -d libstdc++-v3/config/abi/post/sparc64-linux-gnu ]; then
  mkdir -p libstdc++-v3/config/abi/post/sparc64-linux-gnu/64
  mv libstdc++-v3/config/abi/post/sparc64-linux-gnu/{,64/}baseline_symbols.txt
  mv libstdc++-v3/config/abi/post/sparc64-linux-gnu/{32/,}baseline_symbols.txt
  rm -rf libstdc++-v3/config/abi/post/sparc64-linux-gnu/32
fi
%endif

# This test causes fork failures, because it spawns way too many threads
rm -f gcc/testsuite/go.test/test/chan/goroutines.go

# These tests get stuck and don't timeout.
%ifarch ppc ppc64 ppc64le
rm -f libgomp/testsuite/libgomp.c/target-*.c
rm -rf libgomp/testsuite/libgomp.oacc*
rm -rf libgomp/testsuite/libgomp.graphite*
%endif
# This test gets stuck.
%ifarch %{ix86} ppc64 s390x
rm -f libstdc++-v3/testsuite/30_threads/future/members/poll.cc
%endif

%build

# Undo the broken autoconf change in recent Fedora versions
export CONFIG_SITE=NONE

rm -rf obj-%{gcc_target_platform}
mkdir obj-%{gcc_target_platform}
cd obj-%{gcc_target_platform}

%{?scl:PATH=%{_bindir}${PATH:+:${PATH}}}

%if 0%{?rhel} == 8
%ifarch ppc64le
export PATH=/opt/rh/gcc-toolset-12/root/usr/bin:${PATH:+:${PATH}}
%endif
%endif

CC=gcc
CXX=g++
OPT_FLAGS=`echo %{optflags}|sed -e 's/\(-Wp,\)\?-D_FORTIFY_SOURCE=[12]//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-flto=auto//g;s/-flto//g;s/-ffat-lto-objects//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-m64//g;s/-m32//g;s/-m31//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mfpmath=sse/-mfpmath=sse -msse2/g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/ -pipe / /g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-Werror=format-security/-Wformat-security/g'`
%ifarch sparc
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mcpu=ultrasparc/-mtune=ultrasparc/g;s/-mcpu=v[78]//g'`
%endif
%ifarch %{ix86}
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-march=i.86//g'`
%endif
OPT_FLAGS=`echo "$OPT_FLAGS" | sed -e 's/[[:blank:]]\+/ /g'`
# We're going to use the old long double format (double double) until RHEL10.
# Only -static-lib{stdc++,gfortran}/libgcc would work with IEEE double.
# Upstream also uses the old long double format, but Fedora uses the new
# format.  To make things clearer, --with-long-double-format=ibm is used
# explicitly.
CONFIGURE_OPTS="\
	--prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
	--with-bugurl=http://bugzilla.redhat.com/bugzilla \
	--enable-shared --enable-threads=posix --enable-checking=release \
%ifarch ppc64le
	--enable-targets=powerpcle-linux \
%endif
%ifarch ppc64le %{mips} s390x
	--disable-multilib \
%else
	--enable-multilib \
%endif
	--with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions \
	--enable-gnu-unique-object --enable-linker-build-id --with-gcc-major-version-only \
	--enable-libstdcxx-backtrace \
	--with-linker-hash-style=gnu \
	--enable-plugin --enable-initfini-array \
	--disable-libquadmath \
	--disable-libsanitizer \
	--disable-libvtv \
	--disable-libgomp \
	--disable-libitm \
	--disable-libssp \
	--disable-libatomic \
	--disable-libcilkrts \
	--without-isl \
	--disable-libmpx \
%if 0%{?fedora} >= 21 || 0%{?rhel} >= 7
%if %{attr_ifunc}
	--enable-gnu-indirect-function \
%endif
%endif
%ifarch ppc ppc64 ppc64le ppc64p7
	--enable-secureplt \
%endif
%ifarch sparc sparcv9 sparc64 ppc ppc64 ppc64le ppc64p7 s390 s390x alpha
	--with-long-double-128 \
%endif
%ifarch ppc64le
	--with-long-double-format=ibm \
%endif
%ifarch ppc ppc64 ppc64p7
%if 0%{?rhel} >= 7
	--with-cpu-32=power7 --with-tune-32=power7 --with-cpu-64=power7 --with-tune-64=power7 \
%endif
%if 0%{?rhel} == 6
	--with-cpu-32=power4 --with-tune-32=power6 --with-cpu-64=power4 --with-tune-64=power6 \
%endif
%endif
%ifarch ppc64le
%if 0%{?rhel} == 9
	--with-cpu-32=power9 --with-tune-32=power9 --with-cpu-64=power9 --with-tune-64=power9 \
%else
	--with-cpu-32=power8 --with-tune-32=power8 --with-cpu-64=power8 --with-tune-64=power8 \
%endif
%endif
%ifarch ppc
	--build=%{gcc_target_platform} --target=%{gcc_target_platform} --with-cpu=default32
%endif
%ifarch %{ix86} x86_64
%if 0%{?rhel} >= 8
	--enable-cet \
%endif
	--with-tune=generic \
%endif
%if 0%{?rhel} >= 7
%ifarch %{ix86}
	--with-arch=x86-64 \
%endif
%ifarch x86_64
%if 0%{?rhel} > 8
	--with-arch_64=x86-64-v2 \
%endif
	--with-arch_32=x86-64 \
%endif
%else
%ifarch %{ix86}
	--with-arch=i686 \
%endif
%ifarch x86_64
	--with-arch_32=i686 \
%endif
%endif
%ifarch s390 s390x
%if 0%{?rhel} >= 7
%if 0%{?rhel} > 7
%if 0%{?rhel} > 8
%if 0%{?rhel} == 9
	--with-arch=z14 --with-tune=z15 \
%else
	--with-arch=z13 --with-tune=arch13 \
%endif
%else
	--with-arch=z13 --with-tune=z14 \
%endif
%else
	--with-arch=z196 --with-tune=zEC12 \
%endif
%else
%if 0%{?fedora} >= 26
	--with-arch=zEC12 --with-tune=z13 \
%else
	--with-arch=z9-109 --with-tune=z10 \
%endif
%endif
	--enable-decimal-float \
%endif
	--build=%{gcc_target_platform} \
	"

CC="$CC" CXX="$CXX" CFLAGS="$OPT_FLAGS" \
	CXXFLAGS="`echo " $OPT_FLAGS " | sed 's/ -Wall / /g;s/ -fexceptions / /g' \
		  | sed 's/ -Wformat-security / -Wformat -Wformat-security /'`" \
	XCFLAGS="$OPT_FLAGS" TCFLAGS="$OPT_FLAGS" \
	../configure --enable-bootstrap \
	--enable-languages=c,c++,lto \
	$CONFIGURE_OPTS


make %{?_smp_mflags} BOOT_CFLAGS="$OPT_FLAGS"

%install
rm -fr %{buildroot}

%{?scl:PATH=%{_bindir}${PATH:+:${PATH}}}
mkdir -p %{buildroot}%{_root_prefix}/%{_lib}
cd obj-%{gcc_target_platform}
./gcc/xgcc -v 2> %{buildroot}%{_root_prefix}/%{_lib}/README%{gcc_major}

cp %{gcc_target_platform}/libstdc++-v3/src/.libs/libstdc++.so.6.0.* %{buildroot}%{_root_prefix}/%{_lib}/compat-sap-c++-%{gcc_version}.so
cd %{buildroot}%{_root_prefix}/%{_lib}/
ln -sf compat-sap-c++-%{gcc_version}.so %{buildroot}%{_root_prefix}/%{_lib}/compat-sap-c++-%{gcc_major}.so

%check


cd obj-%{gcc_target_platform}

%{?scl:PATH=%{_bindir}${PATH:+:${PATH}}}


# run the tests.
LC_ALL=C make %{?_smp_mflags} -k check ALT_CC_UNDER_TEST=gcc ALT_CXX_UNDER_TEST=g++ \
%if 0%{?fedora} >= 20 || 0%{?rhel} > 7
     RUNTESTFLAGS="--target_board=unix/'{,-fstack-protector-strong}'" || :
%else
%ifnarch ppc ppc64 ppc64le s390x
     RUNTESTFLAGS="--target_board=unix/'{,-fstack-protector}'" || :
%else
    || :
%endif
%endif
( LC_ALL=C ../contrib/test_summary -t || : ) 2>&1 | sed -n '/^cat.*EOF/,/^EOF/{/^cat.*EOF/d;/^EOF/d;/^LAST_UPDATED:/d;p;}' > testresults
rm -rf gcc/testsuite.prev
mv gcc/testsuite{,.prev}
rm -f gcc/site.exp
LC_ALL=C make %{?_smp_mflags} -C gcc -k check-gcc check-g++ ALT_CC_UNDER_TEST=gcc ALT_CXX_UNDER_TEST=g++ RUNTESTFLAGS="--target_board=unix/'{,-fstack-protector}' compat.exp struct-layout-1.exp" || :
mv gcc/testsuite/gcc/gcc.sum{,.sent}
mv gcc/testsuite/g++/g++.sum{,.sent}
( LC_ALL=C ../contrib/test_summary -o -t || : ) 2>&1 | sed -n '/^cat.*EOF/,/^EOF/{/^cat.*EOF/d;/^EOF/d;/^LAST_UPDATED:/d;p;}' > testresults2
rm -rf gcc/testsuite.compat
mv gcc/testsuite{,.compat}
mv gcc/testsuite{.prev,}
echo ====================TESTING=========================
cat testresults
echo ===`gcc --version | head -1` compatibility tests====
cat testresults2
echo ====================TESTING END=====================
mkdir testlogs-%{_target_platform}-%{version}-%{release}
for i in `find . -name \*.log | grep -F testsuite/ | grep -v 'config.log\|acats.*/tests/'`; do
  ln $i testlogs-%{_target_platform}-%{version}-%{release}/ || :
done
for i in `find gcc/testsuite.compat -name \*.log | grep -v 'config.log\|acats.*/tests/'`; do
  ln $i testlogs-%{_target_platform}-%{version}-%{release}/`basename $i`.compat || :
done
tar cf - testlogs-%{_target_platform}-%{version}-%{release} | bzip2 -9c \
  | uuencode testlogs-%{_target_platform}.tar.bz2 || :
rm -rf testlogs-%{_target_platform}-%{version}-%{release}


%files
%defattr(-,root,root,-)
%dir %{_root_prefix}
%dir %{_root_prefix}/%{_lib}
%{_root_prefix}/%{_lib}/compat-sap-c++-%{gcc_version}.so
%{_root_prefix}/%{_lib}/compat-sap-c++-%{gcc_major}.so
%{_root_prefix}/%{_lib}/README%{gcc_major}
%ghost %{_root_prefix}/%{_lib}/libstdc++.so.6

%transfiletriggerin -- %{_root_prefix}/%{_lib}
if ! [ -d %{_root_prefix}/%{_lib} ]; then
  exit 0
fi
symlink=%{_root_prefix}/%{_lib}/libstdc++.so.6
if [ -e $symlink ] && ! [ -L $symlink ]; then
  exit 0
fi
latest_so=$(ls %{_root_prefix}/%{_lib} | grep "^compat-sap-c++-[1-9][0-9]\.so$" | tail -n1)
if [ -n $latest_so ]; then
  ln -sf $latest_so $symlink
fi
%end

%transfiletriggerpostun -- %{_root_prefix}/%{_lib}
if ! [ -d %{_root_prefix}/%{_lib} ]; then
  exit 0
fi
symlink=%{_root_prefix}/%{_lib}/libstdc++.so.6
if [ -e $symlink ] && ! [ -L $symlink ]; then
  exit 0
fi
latest_so=$(ls %{_root_prefix}/%{_lib} | grep "^compat-sap-c++-[1-9][0-9]\.so$" | tail -n1)
if [ -n $latest_so ]; then
  ln -sf $latest_so $symlink
fi
%end

%changelog
* Fri Jan 12 2024 Patrick Palka <ppalka@redhat.com> 12.1.1-1.2
- New build with updated version to avoid version conflict (RHEL-21071)

* Thu Jan 11 2024 Patrick Palka <ppalka@redhat.com> 12.1.1-1.1
- provide /opt/rh/SAP/lib64/libstdc++.so.6 symlink (RHEL-21071)

* Mon May 16 2022 Marek Polacek <polacek@redhat.com> 12.1.1-1
- update to GCC 12 (#2070133)

* Wed Jul 14 2021 Marek Polacek <polacek@redhat.com> 11.1.1-6
- update to GCC 11 (#1957766)

* Wed May 19 2021 Marek Polacek <polacek@redhat.com> 10.2.1-11
- update from Fedora gcc 10.2.1-11 (#1933097)
- apply gcc10-SIZE_MAX.patch

* Wed Jul 29 2020 Marek Polacek <polacek@redhat.com> 10.2.1-1
- GCC 10.2 release
- disable -flto in %%{optflags}, lto bootstrap will be enabled the GCC way
  later
- require MPFR Library version 3.1.0 (or later)

* Thu Jun  4 2020 Marek Polacek <polacek@redhat.com> 10.1.1-1
- new package
